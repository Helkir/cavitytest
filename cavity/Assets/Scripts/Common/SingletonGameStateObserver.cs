﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SDD.Events;
using System;

public abstract class SingletonGameStateObserver<T> :  Singleton<T>,IEventHandler where T:Component
{
	public virtual void SubscribeEvents()
	{
		//EventManager.Instance.AddListener<GameMenuEvent>(GameMenu);
	}

	public virtual void UnsubscribeEvents()
	{
		//EventManager.Instance.RemoveListener<GameMenuEvent>(GameMenu);
	}

	protected override void Awake()
	{
		base.Awake();
		SubscribeEvents();
	}

	protected virtual void OnDestroy()
	{
		UnsubscribeEvents();
	}

    
    protected virtual void GameMenu(GameMenuEvent e)
	{
	}

}
