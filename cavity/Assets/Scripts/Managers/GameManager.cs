﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SDD.Events;

public enum GameState { gameMenu, gamePlay,gameNextLevel,gamePause,gameOver,gameVictory}

public class GameManager : Manager<GameManager> {

	#region Time
	void SetTimeScale(float newTimeScale)
	{
		Time.timeScale = newTimeScale;
	}
	#endregion

	#region Game State
	private GameState m_GameState;
	public bool IsPlaying { get { return m_GameState == GameState.gamePlay; } }
    #endregion

    //[Header("GameManager")]

    #region Death management
    public int m_death_count;
    public void IncrementDeathCount(int count)
    {
        count++;
        SetDeathCount(count);
    }
    public void SetDeathCount(int count)
    {
        m_death_count = count;
        EventManager.Instance.Raise(new GameStatisticsChangedEvent() { eDeathCount = m_death_count });
    }
    #endregion

    #region Events' subscription
    public override void SubscribeEvents()
	{
		base.SubscribeEvents();
        
	}

	public override void UnsubscribeEvents()
	{
		base.UnsubscribeEvents();
        
	}
	#endregion

	#region Manager implementation
	protected override IEnumerator InitCoroutine()
	{
		EventManager.Instance.Raise(new GameStatisticsChangedEvent() { eDeathCount = m_death_count });
		yield break;
	}
	#endregion

	#region Game flow & Gameplay
	//Game initialization
	void InitNewGame()
	{
        SetDeathCount(0);
	}
	#endregion
    
	
}
