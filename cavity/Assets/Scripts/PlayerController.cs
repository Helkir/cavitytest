﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SDD.Events;
using UnityEngine;

namespace Assets.Scripts
{
    class PlayerController: SimpleGameStateObserver
    {
        public float speed = 0.8f;
        private int m_death_count = 0;
        private SpriteRenderer sprite;

        private void Start()
        {
            sprite = GetComponent<SpriteRenderer>();
        }

        private void Update()
        {
            float translation = Input.GetAxis("Horizontal");
            if (translation > 0)
            {
                sprite.flipX = true;
            } else if (translation < 0)
            {
                sprite.flipX = false;
            }
            transform.Translate(translation * speed * Time.deltaTime, 0, 0);
        }

        private void OnTriggerEnter2D(UnityEngine.Collider2D other)
        {
            if (other.CompareTag("Trap"))
            {
                m_death_count++;
                EventManager.Instance.Raise(new DeathCountChangedEvent());   
            }
        }
    }
}
